<?php
//Repetition Control Structures

	//While loop: takes a single condition. If the condition evaluates to true, the code inside the block will run.
		//Checks the condition before executing.
		function whileLoop(){
			$count = 5;

			while($count !== 0){
				echo $count . '<br/>';
				$count--;
			}
		}


	//Do While Loop: guarantees that the code will be executed at least once.
		//Executes first before checks the condition.
		function doWhileLoop(){
			$count = 20;

			do {
				echo $count . '<br/>';
				$count--;
			} while ($count > 0);
		}


	//For Loop: more flexible than while and do-while loops.
		function forLoop(){
			for ($count = 0; $count <= 10; $count++){
				echo $count . '<br/>';
			}
		}


	//Array Manipulation
		//PHP Arrays are declared using square brackets '[]' or array() function.
		$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'); //was used before PHP 5.4

		$newStudentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; //introduced during PHP 5.4 update

		//Simple Array
			//Array -> is a collection of related data.
			$grades = [98.5, 94.3, 89.2, 90.1];
			$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba'];
			$task = [
				'drink html',
				'eat javascript',
				'inhale css',
				'bake sass'
			];

		//Associative Array -> we associate property values in such way that it is still considered an array, rather than object.
			$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 90.1, 'thirdGrading' => 94.3, 'fourthGrading' => 95.4];
?>